package com.hotmail.fabiansandberg98.commands;


import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.ultrawarnings;

public class listwarnings implements CommandExecutor {
	
	ultrawarnings plugin;
	public listwarnings(ultrawarnings plugin) {
		this.plugin = plugin;
	}
	
	
	public int CurrentPoints(OfflinePlayer target) {
		if (SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId()) == null) {
			return 0;
		}
		return SettingsManager.getInstance().getData().getInt("uuid." + target.getUniqueId() + ".points");
	}
	
	public int MaxPoints() {
		if (plugin.getConfig().getInt("max_points") == 0) {
			return 0;
		}
		return plugin.getConfig().getInt("max_points");
	}
	
	public int WarningSize(OfflinePlayer target) {
		if (SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId().toString() + ".warnings") == null) {
			return 0;
		}
		return SettingsManager.getInstance().getData().getConfigurationSection("uuid." + target.getUniqueId().toString() + ".warnings").getKeys(false).size();
	}
	
	public String MagicPointsLevel(OfflinePlayer target) {
		
		int OneOfFive = MaxPoints() / 5;
		int TwoOfFive = OneOfFive*2;
		int ThreeOfFive = OneOfFive*3;
		int FourOfFive = TwoOfFive*2;
		int FiveOfFive = MaxPoints();
		
		if (CurrentPoints(target) <= OneOfFive) {
			return ChatColor.DARK_GREEN + "Very good";
		}
		
		if (CurrentPoints(target) > OneOfFive && CurrentPoints(target) <= TwoOfFive) {
			return ChatColor.GREEN + "Good";
		}
		
		if (CurrentPoints(target) > TwoOfFive && CurrentPoints(target) <= ThreeOfFive) {
			return ChatColor.YELLOW + "Average";
		}
		
		if (CurrentPoints(target) > ThreeOfFive && CurrentPoints(target) <= FourOfFive) {
			return ChatColor.RED + "Poor";
		}
		
		if (CurrentPoints(target) > FourOfFive && CurrentPoints(target) <= FiveOfFive) {
			return ChatColor.DARK_RED + "Very poor";
		}
		
		return " \"Error\" ";
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command has been disabled for CONSOLE users");
			return true;
		}
		
		Player player = (Player) sender;
		
		if (!player.hasPermission("ultrawarnings.listwarnings")) {
			player.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command!");
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length == 0) {
			
			/*
			 * Make sure the target has warnings
			 */
			if (SettingsManager.getInstance().getData().getString("uuid." + player.getUniqueId().toString()) == null
					|| SettingsManager.getInstance().getData().getConfigurationSection("uuid." + player.getUniqueId().toString() + ".warnings").getKeys(false).size() <= 0) {
				player.sendMessage(ChatColor.DARK_GREEN + "--- " + ChatColor.WHITE + "Your Warnings List" + ChatColor.DARK_GREEN + " ---"
						+ "\nYou got " + ChatColor.WHITE + this.CurrentPoints(player) + "/" + this.MaxPoints() + ChatColor.DARK_GREEN + " points,"
								+ " and a total of " + ChatColor.WHITE + this.WarningSize(player) + ChatColor.DARK_GREEN + " warnings."
								+ "\nYour warning level is " + ChatColor.RESET + this.MagicPointsLevel(player));
				return true;
			}
			
			player.sendMessage(ChatColor.DARK_GREEN + "--- " + ChatColor.WHITE + "Your Warnings List" + ChatColor.DARK_GREEN + " ---"
					+ "\nYou got " + ChatColor.WHITE + this.CurrentPoints(player) + "/" + this.MaxPoints() + ChatColor.DARK_GREEN + " points,"
							+ " and a total of " + ChatColor.WHITE + this.WarningSize(player) + ChatColor.DARK_GREEN + " warnings."
							+ "\nYour warning level is " + ChatColor.RESET + this.MagicPointsLevel(player));
			
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length > 2) {
			player.sendMessage(ChatColor.RED + "Wrong usage! Type: /listwarnings or /listwarnings <player>");
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length == 1) {
			
			if (!player.hasPermission("ultrawarnings.listwarnings.others")) {
				player.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command!");
				return true;
			}
			
			@SuppressWarnings("deprecation")
			// Player (target)
			final OfflinePlayer target = this.plugin.getServer().getOfflinePlayer(args[0]);
			
			
			/*
			 * Make sure the target is not null
			 */
			if (target == null) {
				player.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			
			/*
			 * Make sure the target has warnings
			 */
			if (SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId().toString()) == null
					|| SettingsManager.getInstance().getData().getConfigurationSection("uuid." + target.getUniqueId().toString() + ".warnings").getKeys(false).size() <= 0) {
				player.sendMessage(ChatColor.DARK_GREEN + "--- " + ChatColor.WHITE + target.getName() + "'s Warnings List" + ChatColor.DARK_GREEN + " ---"
						+ "\n" + target.getName() + " got " + ChatColor.WHITE + this.CurrentPoints(target) + "/" + this.MaxPoints() + ChatColor.DARK_GREEN + " points,"
								+ " and a total of " + ChatColor.WHITE + this.WarningSize(target) + ChatColor.DARK_GREEN + " warnings."
								+ "\n" + target.getName() + "'s warning level is " + ChatColor.RESET + this.MagicPointsLevel(target));
				return true;
			}
			
			player.sendMessage(ChatColor.DARK_GREEN + "--- " + ChatColor.WHITE + target.getName() + "'s Warnings List" + ChatColor.DARK_GREEN + " ---"
					+ "\n" + target.getName() + " got " + ChatColor.WHITE + this.CurrentPoints(target) + "/" + this.MaxPoints() + ChatColor.DARK_GREEN + " points,"
							+ " and a total of " + ChatColor.WHITE + this.WarningSize(target) + ChatColor.DARK_GREEN + " warnings."
							+ "\n" + target.getName() + "'s warning level is " + ChatColor.RESET + this.MagicPointsLevel(target)
							+ "\n" + getWarnings(target, 1));
			return true;
		}
		
		if (args.length == 2) {
			String page = args[1];
			
			if (page.matches("^.*[^0-9 ].*$")) {
				player.sendMessage(ChatColor.RED + "That is not a valid page!");
			}
			
			if (!player.hasPermission("ultrawarnings.listwarnings.others")) {
				player.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command!");
				return true;
			}
			
			@SuppressWarnings("deprecation")
			// Player (target)
			final OfflinePlayer target = this.plugin.getServer().getOfflinePlayer(args[0]);
			
			
			/*
			 * Make sure the target is not null
			 */
			if (target == null) {
				player.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			
			/*
			 * Make sure the target has warnings
			 */
			if (SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId().toString()) == null
					|| SettingsManager.getInstance().getData().getConfigurationSection("uuid." + target.getUniqueId().toString() + ".warnings").getKeys(false).size() <= 0) {
				player.sendMessage(ChatColor.DARK_GREEN + "--- " + ChatColor.WHITE + target.getName() + "'s Warnings List" + ChatColor.DARK_GREEN + " ---"
						+ "\n" + target.getName() + " got " + ChatColor.WHITE + this.CurrentPoints(target) + "/" + this.MaxPoints() + ChatColor.DARK_GREEN + " points,"
								+ " and a total of " + ChatColor.WHITE + this.WarningSize(target) + ChatColor.DARK_GREEN + " warnings."
								+ "\n" + target.getName() + "'s warning level is " + ChatColor.RESET + this.MagicPointsLevel(target));
				return true;
			}
			if (page.equalsIgnoreCase("1")) {
				player.sendMessage(ChatColor.DARK_GREEN + "--- " + ChatColor.WHITE + target.getName() + "'s Warnings List" + ChatColor.DARK_GREEN + " ---"
						+ "\n" + target.getName() + " got " + ChatColor.WHITE + this.CurrentPoints(target) + "/" + this.MaxPoints() + ChatColor.DARK_GREEN + " points,"
								+ " and a total of " + ChatColor.WHITE + this.WarningSize(target) + ChatColor.DARK_GREEN + " warnings."
								+ "\n" + target.getName() + "'s warning level is " + ChatColor.RESET + this.MagicPointsLevel(target)
								+ "\n" + getWarnings(target, 1));
				return true;
			}
			
			player.sendMessage(getWarnings(target, Integer.parseInt(page)));
			return true;
			
		}
		
		return false;
	}
	
	public String getWarnings(OfflinePlayer target, int page) {
		StringBuilder builder = new StringBuilder();
		int pageCalc = page - 1;
		double totalPages = (((SettingsManager.getInstance().getData().getConfigurationSection("uuid." + target.getUniqueId().toString() + ".warnings").getKeys(false).size() % 3) == 0) ? 
				SettingsManager.getInstance().getData().getConfigurationSection("uuid." + target.getUniqueId().toString() + ".warnings").getKeys(false).size() / 3
				: (SettingsManager.getInstance().getData().getConfigurationSection("uuid." + target.getUniqueId().toString() + ".warnings").getKeys(false).size() / 3) + 1);
		int x = 3 * (pageCalc - 1);
		int y = x + 3;
		for(String warnings : SettingsManager.getInstance().getData().getConfigurationSection("uuid." + target.getUniqueId().toString() + ".warnings").getKeys(false)) {
			
		if(x > 0) {
			x--;
			y--;
		continue;
		}

		if(y == 0) {
		break;
		}
		
		UUID warnerUUID = UUID.fromString(SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId().toString() + ".warnings." + warnings + ".warner"));
		Player fromID = Bukkit.getPlayer(warnerUUID);
		
		builder.append(ChatColor.DARK_GREEN + "--- " + ChatColor.WHITE + warnings + ChatColor.DARK_GREEN + " ---" +
				"\nPoints: " + ChatColor.WHITE + SettingsManager.getInstance().getData().getInt("uuid." + target.getUniqueId().toString() + ".warnings." + warnings + ".points") + ChatColor.DARK_GREEN +
				"\nWarner: " + ChatColor.WHITE + fromID.getName() + ChatColor.DARK_GREEN +
				"\nReason: " + ChatColor.WHITE + SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId().toString() + ".warnings." + warnings + ".reason"));
		builder.append("\n");
		x--;
		y--;

		}
		return builder.toString().trim() + ChatColor.DARK_GREEN +
				"\nPage (" + ChatColor.WHITE + (pageCalc + 1) + " of " + Integer.valueOf((int)totalPages+1) + ChatColor.DARK_GREEN + ")";
	}
}
