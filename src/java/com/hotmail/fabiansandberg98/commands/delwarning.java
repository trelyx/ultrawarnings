package com.hotmail.fabiansandberg98.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.ultrawarnings;

public class delwarning implements CommandExecutor {
	
	ultrawarnings plugin;
	public delwarning(ultrawarnings plugin) {
		this.plugin = plugin;
	}
	
	public Player UUIDtoPlayer() {
		UUID getID = UUID.fromString("");
		Player fromID = Bukkit.getPlayer(getID);
		return fromID;
	}
	
	public int UpdatePoints(OfflinePlayer player, int parseInt) {
		if (CurrentPoints(player) <= 0) {
			return 0;
		}
		return CurrentPoints(player) - parseInt;
	}
	
	public int CurrentPoints(OfflinePlayer player) {
		if (SettingsManager.getInstance().getData().getString("uuid." + player.getUniqueId()) == null) {
			return 0;
		}
		return SettingsManager.getInstance().getData().getInt("uuid." + player.getUniqueId() + ".points");
	}
	
public void MagicConfig(OfflinePlayer target, int id, int updatePoints) {
		
		SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".warnings." + id, null);
		SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".points", updatePoints);
		SettingsManager.getInstance().saveData();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command has been disabled for CONSOLE users");
			return true;
		}
		
		Player player = (Player) sender;
		
		if (!player.hasPermission("ultrawarnings.delwarning")) {
			player.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command!");
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length == 0) {
			player.sendMessage(ChatColor.RED + "Wrong usage! Type: /delwarning <player> <warning>");
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length == 1) {
			player.sendMessage(ChatColor.RED + "Wrong usage! Type: /delwarning <player> <warning>");
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length > 2) {
			player.sendMessage(ChatColor.RED + "Wrong usage! Type: /delwarning <player> <warning>");
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length == 2) {
			
			@SuppressWarnings("deprecation")
			// Player (target)
			final OfflinePlayer target = this.plugin.getServer().getOfflinePlayer(args[0]);
			
			String ID = args[1];
			
			
			/*
			 * Make sure the target is not null
			 */
			if (target == null) {
				player.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			
			/*
			 * Make sure the target has warnings
			 */
			if (SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId()) == null) {
				player.sendMessage(ChatColor.RED + "That player has no warnings!");
				return true;
			}
				
			
			/*
			 * Make sure the ID is correct
			 */
			if (ID == null) {
				player.sendMessage(ChatColor.RED + "That warning does not exist! Make sure you typed in the proper Id!");
				return true;
			}
			
			/*
			 * Make sure the ID contains only numbers
			 */
			if (ID.matches("^.*[^0-9 ].*$")) {
				player.sendMessage(ChatColor.RED + "Make sure the Id contains numbers only!");
				return true;
			}
			
			/*
			 * Make sure the warning exists
			 */
			if (SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId() + ".warnings." + ID) == null) {
				player.sendMessage(ChatColor.RED + "That warning does not exist");
				return true;
			}
			
			int getId = Integer.parseInt(ID); 
			int points = SettingsManager.getInstance().getData().getInt("uuid." + target.getUniqueId().toString() + ".warnings." + getId + ".points");
			
			player.sendMessage(ChatColor.DARK_GREEN + "You removed a warning from " + target.getName() + "."
					+ "\n" + points + "/" + this.CurrentPoints(target) + " were removed.");
			
			this.MagicConfig(target, getId, this.UpdatePoints(target, points));
			
			
			/*
			 * Send target a message
			 */
			if (target.isOnline()) {
				//Message
				target.getPlayer().sendMessage(ChatColor.GREEN + player.getName() +  " removed a warning!");
				return false;
			}
			
			
			
			return true;
		}
		
		return false;
	}

}
