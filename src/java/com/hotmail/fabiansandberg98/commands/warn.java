package com.hotmail.fabiansandberg98.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.ultrawarnings;
import com.hotmail.fabiansandberg98.CleanUp.Warn;

public class warn implements CommandExecutor {
	
	ultrawarnings plugin;
	Warn handleWarn = new Warn(plugin);
	
	public warn(ultrawarnings plugin) {
		this.plugin = plugin;
	}

	/*
	 * Warn a player
	 * See handleWarnCommand.java for more of the code
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command has been disabled for CONSOLE users");
			return true;
		}
		
		Player player = (Player) sender;
		
		if (!player.hasPermission("ultrawarnings.warn")) {
			player.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command!");
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length < 2) {
			player.sendMessage(ChatColor.RED + "Wrong usage! Type: /warn <player> <points> <reason>");
			return true;
		}
		
		/*
		 * Make sure the command is executed the right way
		 */
		if (args.length > 2) {
			
			@SuppressWarnings("deprecation")
			// Player (target)
			final OfflinePlayer target = this.plugin.getServer().getOfflinePlayer(args[0]);
			
			/*
			 * Make sure the target is not null (Should not be null, we're using OfflinePlayer)
			 */
			if (target == null) {
				player.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			
			/*
			 * Make sure that the target is not the player
			 *//*
			if (target == player) {
				player.sendMessage(ChatColor.WHITE + "NOPE!");
				return true;
			}*/
			
			/*
			 * Make sure the points contains numbers only
			 */
			if (handleWarn.pointsContainsLetters(args)) {
				player.sendMessage(ChatColor.RED + "Make sure the amount of points containts numbers only!");
				return true;
			}
			
			int id        = handleWarn.getRandom(target);
			int points    = handleWarn.convertPoints(args);
			int available = handleWarn.AvailablePoints(target, plugin.getConfig());
			int rawpoints = handleWarn.RawPoints(target, points, plugin.getConfig());
			int add       = handleWarn.AddPointsToAmountOfCurrentPoints(target, points, plugin.getConfig());
			String reason = handleWarn.reason(args);
			
			player.sendMessage(ChatColor.DARK_GREEN + "You warned " + target.getName() + "."
					+ "\n" + rawpoints + "/" + available + " points were given!"
							+ "\nReason: " + reason);
			
			handleWarn.setWarningData(player, target, add, rawpoints, reason, id);
			
			
			/*
			 * If target is online, send a message
			 */
			if (target.isOnline()) {
				//Message
				target.getPlayer().sendMessage(ChatColor.RED + "You got warned by " + player.getName() + "."
						+ "\nReason: " + reason);
			}
			
			/*
			 * Ban manager
			 * 
			 */
			if (handleWarn.CheckForBan(target, plugin.getConfig())) {
				
				@SuppressWarnings("deprecation")
				Player[] all = Bukkit.getServer().getOnlinePlayers();
				
				for (Player players : all) {
					if (players.hasPermission("ultrawarnings.banmessage")) {
						
						players.sendMessage(ChatColor.RED + target.getName() + " reached " + handleWarn.getBanPoints() + " points and was banned " + handleWarn.getBanMessage() + "!");
						return true;
					}
				}
			}
			
			return true;
		}
		
		return false;
	}
}
