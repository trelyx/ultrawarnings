package com.hotmail.fabiansandberg98.CleanUp;

import java.util.Random;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.ultrawarnings;

public class Warn {

	ultrawarnings plugin;
	public Warn(ultrawarnings plugin) {
		this.plugin = plugin;
	}
	
	/*
	 * Gets the amount of points target has
	 */
	public int AmountOfCurrentPoints(OfflinePlayer target) {
		if (SettingsManager.getInstance().getData().getString("uuid." + target.getUniqueId()) == null) {
			return 0;
		}
		return SettingsManager.getInstance().getData().getInt("uuid." + target.getUniqueId() + ".points");
	}
	
	/*
	 * Gets the amount of available points to give target
	 */
	public int AvailablePoints(OfflinePlayer target, Configuration config) {
		return this.MaxPoints(config) - this.AmountOfCurrentPoints(target);
	}
	
	/*
	 * Adds points to AmountOfCurrentPoints
	 */
	public int AddPointsToAmountOfCurrentPoints(OfflinePlayer target, int parseInt, Configuration config) {
		
		if (AmountOfCurrentPoints(target) + parseInt > MaxPoints(config)) {
			return MaxPoints(config);	
		}
		
		return this.AmountOfCurrentPoints(target) + parseInt;
	}
	
	/*
	 * Get the raw points player is giving target
	 */
	public int RawPoints(OfflinePlayer target, int parseInt, Configuration config) {
		
		if (AmountOfCurrentPoints(target) == MaxPoints(config)) {
			return 0;
		}	
		
		if (AmountOfCurrentPoints(target) + parseInt > MaxPoints(config)) {
			return this.MaxPoints(config) - this.AmountOfCurrentPoints(target);
		}
		
		return parseInt;
	}
	
	/*
	 * Adds Warning Data to data.yml
	 */
	public void setWarningData(Player player, OfflinePlayer target, int points, int rawpoints, String reason, int id) {
		
		SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".points", points);
		SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".warnings." + id + ".points", rawpoints);
		SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".warnings." + id + ".reason", reason);
		SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".warnings." + id + ".warner", player.getUniqueId().toString());
		SettingsManager.getInstance().saveData();
	}
	
	
	/*
	 * Max Amount of points a player can have
	 */
	public int MaxPoints(Configuration config) {
		if (!config.contains("points_to_one_hour_ban")) {
			return 200;
		}
		return config.getInt("max_points");
	}
	
	/*
	 * Max Amount of points a player can have before one day ban
	 */
	public int points_to_one_day_ban(Configuration config) {
		if (!config.contains("points_to_one_hour_ban")) {
			return 50;
		}
		return config.getInt("points_to_one_day_ban");
	}
	
	/*
	 * Max Amount of points a player can have before one hour ban
	 */
	public int points_to_one_hour_ban(Configuration config) {
		if (!config.contains("points_to_one_hour_ban")) {
			return 35;
		}
		return config.getInt("points_to_one_hour_ban");
	}
	
	/*
	 * Max Amount of points a player can have before one week ban
	 */
	public int points_to_one_week_ban(Configuration config) {
		if (!config.contains("points_to_one_hour_ban")) {
			return 80;
		}
		return config.getInt("points_to_one_week_ban");
	}
	
	/*
	 * Max Amount of points a player can have before one month ban
	 */
	public int points_to_one_month_ban(Configuration config) {
		if (!config.contains("points_to_one_hour_ban")) {
			return 130;
		}
		return config.getInt("points_to_one_month_ban");
	}
	
	/*
	 * Manage Ban
	 */
	
	String banmessage = "";
	int banpoints = 0;
	public void setBanMessage(String message, int points) {
		this.banmessage = message;
		this.banpoints = points;
	}
	
	public String getBanMessage() {
		return this.banmessage;
	}
	
	public int getBanPoints() {
		return this.banpoints;
	}
	
	public boolean CheckForBan(OfflinePlayer target, Configuration config) {
		if (!SettingsManager.getInstance().getData().contains("uuid." + target.getUniqueId().toString() + ".bans.OneHour") || SettingsManager.getInstance().getData().getBoolean("uuid." + target.getUniqueId().toString() + ".bans.OneHour") == false) {
		
			if (AmountOfCurrentPoints(target) >= points_to_one_hour_ban(config)) {
				setBanMessage("for one hour", points_to_one_hour_ban(config));
				SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".bans.OneHour", true);
				SettingsManager.getInstance().saveData();
				return true;
			}
		}
	
	/*
	 * If player has reached one Day Ban
	 */
		if (!SettingsManager.getInstance().getData().contains("uuid." + target.getUniqueId().toString() + ".bans.OneDay") || SettingsManager.getInstance().getData().getBoolean("uuid." + target.getUniqueId().toString() + ".bans.OneDay") == false) {
		
			if (AmountOfCurrentPoints(target) >= points_to_one_day_ban(config)) {
				setBanMessage("for one day", points_to_one_day_ban(config));
				SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".bans.OneDay", true);
				SettingsManager.getInstance().saveData();
				return true;
			}
		}
	
	/*
	 * If player has reached one Week Ban
	 */
		if (!SettingsManager.getInstance().getData().contains("uuid." + target.getUniqueId().toString() + ".bans.OneWeek") || SettingsManager.getInstance().getData().getBoolean("uuid." + target.getUniqueId().toString() + ".bans.OneWeek") == false) {
		
			if (AmountOfCurrentPoints(target) >= points_to_one_week_ban(config)) {
				setBanMessage("for one week", points_to_one_week_ban(config));
				SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".bans.OneWeek", true);
				SettingsManager.getInstance().saveData();
				return true;
			}
		}
		
	/*
	 * If player has reached one Month Ban
	 */
		if (!SettingsManager.getInstance().getData().contains("uuid." + target.getUniqueId().toString() + ".bans.OneMonth") || SettingsManager.getInstance().getData().getBoolean("uuid." + target.getUniqueId().toString() + ".bans.OneMonth") == false) {
		
			if (AmountOfCurrentPoints(target) >= points_to_one_month_ban(config)) {
				setBanMessage("for one month", points_to_one_month_ban(config));
				SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".bans.OneMonth", true);
				SettingsManager.getInstance().saveData();
				return true;
			}
		}
	
	/*
	 * If player has reached Max Points
	 */
		if (!SettingsManager.getInstance().getData().contains("uuid." + target.getUniqueId().toString() + ".bans.Banned") || SettingsManager.getInstance().getData().getBoolean("uuid." + target.getUniqueId().toString() + ".bans.Banned") == false) {
		
			if (AmountOfCurrentPoints(target) == MaxPoints(config)) {
				setBanMessage("permanently", MaxPoints(config));
				SettingsManager.getInstance().getData().set("uuid." + target.getUniqueId().toString() + ".bans.Banned", true);
				SettingsManager.getInstance().saveData();
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Get the reason for the warning
	 */
	public String reason(String[] args) {
		StringBuilder builder = new StringBuilder();
		for (int i = 2; i < args.length; i++) {
		builder.append(args[i]);
		builder.append(" ");
		}
		return builder.toString().trim();
	}
	
	/*
	 * Creates a random 'ID' for the Warning
	 * Could sort it out from 1 to 9, but that ain't fun
	 */
	public int getRandom(OfflinePlayer target) {
        Random random = new Random();
        int id;
        
        id = random.nextInt((9999 - 0) + 1) + 0;
        
        if (SettingsManager.getInstance().getData().contains("uuid." + target.getUniqueId().toString() + ".warnings." + id)) {
			id = random.nextInt((9999 - 0) + 1) + 0;
			return id;
		}
        
        return id;
    }
	
	/*
	 * Convert string to int
	 */
	public int convertPoints(String[] args) {
		return Integer.parseInt(args[1]);
	}
	
	/*
	 * Make sure the points only contains numbers
	 */
	public boolean pointsContainsLetters(String[] args) {
		return args[1].matches("^.*[^0-9 ].*$");
	}
	
}
