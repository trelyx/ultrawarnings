package com.hotmail.fabiansandberg98;

import org.bukkit.plugin.java.JavaPlugin;

import com.hotmail.fabiansandberg98.commands.delwarning;
import com.hotmail.fabiansandberg98.commands.listwarnings;
import com.hotmail.fabiansandberg98.commands.warn;

public class ultrawarnings extends JavaPlugin {
	
	@Override
	public void onEnable() {
		getLogger().info("onEnable has been started!");
		getConfig().options().copyDefaults(true);
		saveConfig();
		SettingsManager.getInstance().setup(this);
		getCommands();
	}

	@Override
	public void onDisable() {
		getLogger().info("onDisable has been started!");
	}
	
	public void getCommands() {
		warn warn = new warn(this);
		getCommand("warn").setExecutor(warn);
		
		delwarning delwarning = new delwarning(this);
		getCommand("delwarning").setExecutor(delwarning);
		
		listwarnings listwarnings = new listwarnings(this);
		getCommand("listwarnings").setExecutor(listwarnings);
	}

}
